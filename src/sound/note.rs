#![allow(dead_code)]

pub type Note = (Height, Octave, Duration);

pub const NOTES_PER_SEC: u64 = 7;

// S is Sharp
#[derive(Clone, Copy)]
pub enum Height {
	P = 1, // Pause
	C = 523,
	CS = 554,
	D = 587,
	DS = 622,
	E = 659,
	F = 699,
	FS = 740,
	G = 784,
	GS = 930,
	A = 880,
	AS = 932,
	B = 988,
}

#[derive(Clone, Copy)]
pub enum Octave {
	Five = 1,
	Six = 2,
	Seven = 4,
	Eight = 8,
}

#[derive(Clone, Copy)]
pub enum Volume {
	Full = 75,
	Mid = 50,
	Low = 25,
}

#[derive(Clone, Copy)]
pub enum Duration {
	Full = 1,
	Half = 2,
	Third = 3,
	Quarter = 4,
	Eighth = 8,
}
