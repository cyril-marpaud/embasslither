use super::note::{Duration::*, Height::*, Note, Octave::*};

pub type Partition = &'static [Note];

pub const BOOP: Partition = &[(G, Six, Eighth)];
pub const BEEP: Partition = &[(C, Seven, Quarter)];

pub const WIN: Partition = &[
	(C, Six, Quarter),
	(E, Six, Quarter),
	(G, Six, Quarter),
	(E, Six, Quarter),
	(G, Six, Quarter),
	(C, Seven, Full),
];

pub const LOOSE: Partition = &[
	(E, Five, Quarter),
	(D, Five, Quarter),
	(C, Five, Quarter),
	(D, Five, Quarter),
	(C, Five, Full),
];

#[allow(dead_code)]
pub const MARIO_THEME: Partition = &[
	(E, Six, Full),
	(E, Six, Full),
	(P, Six, Full),
	(E, Six, Full),
	(P, Six, Full),
	(C, Six, Full),
	(E, Six, Full),
	(P, Six, Full),
	(G, Six, Full),
	(P, Six, Full),
	(P, Six, Full),
	(P, Six, Full),
	(G, Five, Full),
	(P, Six, Full),
	(P, Six, Full),
	(P, Six, Full),
	(C, Seven, Full),
	(P, Seven, Full),
	(P, Seven, Full),
	(G, Six, Full),
	(P, Seven, Full),
	(P, Seven, Full),
	(E, Six, Full),
	(P, Seven, Full),
	(P, Seven, Full),
	(A, Six, Full),
	(P, Seven, Full),
	(B, Six, Full),
	(P, Seven, Full),
	(AS, Six, Full),
	(A, Six, Full),
	(P, Six, Quarter),
	(G, Six, Full),
	(P, Six, Full),
	(E, Six, Full),
	(G, Six, Full),
	(P, Six, Eighth),
	(A, Six, Full),
	(P, Six, Half),
	(F, Six, Full),
	(G, Six, Full),
	(P, Six, Half),
	(E, Six, Full),
	(P, Six, Half),
	(C, Six, Full),
	(D, Six, Full),
	(B, Five, Full),
];
