#![no_std]
#![no_main]

mod control;
mod display;
mod game;
mod sound;

use defmt::unwrap;
use defmt_rtt as _;
use embassy_executor::Spawner;
use embassy_nrf::{bind_interrupts, gpio::Pin, peripherals::RNG};
use panic_probe as _;

bind_interrupts!(struct Irqs {
	   RNG => embassy_nrf::rng::InterruptHandler<RNG>;
});

#[embassy_executor::main]
async fn main(spawner: Spawner) {
	let p = embassy_nrf::init(Default::default());

	let (btn_a, btn_b, touch) = (p.P0_14.degrade(), p.P0_23.degrade(), p.P1_04.degrade());
	unwrap!(spawner.spawn(control::task(btn_a, btn_b, touch)));

	let rows = [
		p.P0_21.degrade(),
		p.P0_22.degrade(),
		p.P0_15.degrade(),
		p.P0_24.degrade(),
		p.P0_19.degrade(),
	];
	let cols = [
		p.P0_28.degrade(),
		p.P0_11.degrade(),
		p.P0_31.degrade(),
		p.P1_05.degrade(),
		p.P0_30.degrade(),
	];
	unwrap!(spawner.spawn(display::task(rows, cols)));

	let (spkr, pwm) = (p.P0_00.degrade(), p.PWM0);
	unwrap!(spawner.spawn(sound::task(spkr, pwm)));

	let rng = p.RNG;
	unwrap!(spawner.spawn(game::task(rng)));
}
