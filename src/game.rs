mod food;
mod snake;
mod state;

use core::{self, iter::once};

use defmt::{unwrap, Format};
use embassy_futures::select::{select, Either};
use embassy_nrf::{peripherals::RNG, rng::Rng};
use embassy_time::{Duration, Ticker, Timer};

use self::{food::Food, snake::Snake, state::State};
use crate::{
	control::{Button, BTN_CHAN, TOUCH_SIG},
	display::{
		image::{Image, IMG_SIG},
		images::{self, *},
	},
	sound::{partition, SND_SIG},
	Irqs,
};

#[derive(Debug, Default, Eq, Format, PartialEq)]
pub struct Coords {
	x: usize,
	y: usize,
}

impl Coords {
	pub const CENTER: Self = Self { x: 2, y: 2 };
}

struct Game {
	state: State,
	snake: Snake,
	food: Food,
}

impl Game {
	pub fn new(rng: RNG) -> Self {
		Self {
			state: Default::default(),
			snake: Default::default(),
			food: Food::new(Rng::new(rng, Irqs)),
		}
	}

	pub async fn play(&mut self) {
		self.waiting_state().await;
		self.ongoing_state().await;
		self.over_state().await;
	}

	async fn waiting_state(&mut self) {
		let mut images = [UP_ARROW, BLANK].into_iter().cycle();
		let mut ticker = Ticker::every(Duration::from_millis(500));

		while let Either::First(_) = select(ticker.next(), TOUCH_SIG.wait()).await {
			IMG_SIG.signal(unwrap!(images.next()));
		}

		SND_SIG.signal(partition::BOOP);
		for img in [THREE, TWO, ONE] {
			IMG_SIG.signal(img);
			Timer::after_secs(1).await;
			SND_SIG.signal(partition::BOOP);
		}

		self.state = State::Ongoing
	}

	async fn ongoing_state(&mut self) {
		let mut ticker = Ticker::every(Duration::from_millis(200));

		loop {
			let mut img = Image::default();

			self.snake
				.iter()
				.chain(once(&*self.food))
				.for_each(|&Coords { x, y }| img[x][y] = 1);

			IMG_SIG.signal(img);
			ticker.next().await;

			img[self.food.x][self.food.y] = 0;
			IMG_SIG.signal(img);
			ticker.next().await;

			while let Ok(btn) = BTN_CHAN.try_receive() {
				match btn {
					Button::A => self.snake.turn_left(),
					Button::B => self.snake.turn_right(),
				};
			}

			if let Some(new_state) = self.snake.slither(&mut self.food) {
				self.state = new_state;
				break;
			};
		}
	}

	async fn over_state(&mut self) {
		let (img, snd) = match self.state {
			State::Lost => (images::SAD, partition::LOOSE),
			State::Won => (images::HAPPY, partition::WIN),
			_ => panic!(),
		};

		IMG_SIG.signal(img);
		SND_SIG.signal(snd);

		Timer::after_secs(3).await;

		self.reset();
	}

	fn reset(&mut self) {
		self.snake = Snake::default();
		self.food.reset();
		self.state = State::Waiting
	}
}

#[embassy_executor::task]
pub async fn task(rng: RNG) {
	let mut game = Game::new(rng);

	loop {
		game.play().await;
	}
}
