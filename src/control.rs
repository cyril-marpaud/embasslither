use embassy_futures::select::{select3, Either3};
use embassy_nrf::gpio::{AnyPin, Input, Pull};
use embassy_sync::{
	blocking_mutex::raw::CriticalSectionRawMutex, channel::Channel, signal::Signal,
};
use embassy_time::Timer;

pub static BTN_CHAN: Channel<CriticalSectionRawMutex, Button, 10> = Channel::new();
pub static TOUCH_SIG: Signal<CriticalSectionRawMutex, ()> = Signal::new();

pub enum Button {
	A,
	B,
}

#[embassy_executor::task]
pub async fn task(btn_a: AnyPin, btn_b: AnyPin, touch: AnyPin) {
	let mut btn_a = Input::new(btn_a, Pull::Up);
	let mut btn_b = Input::new(btn_b, Pull::Up);
	let mut touch = Input::new(touch, Pull::None);

	loop {
		match select3(
			btn_a.wait_for_falling_edge(),
			btn_b.wait_for_falling_edge(),
			touch.wait_for_falling_edge(),
		)
		.await
		{
			Either3::First(_) => BTN_CHAN.send(Button::A).await,
			Either3::Second(_) => BTN_CHAN.send(Button::B).await,
			Either3::Third(_) => TOUCH_SIG.signal(()),
		}

		// Debounce buttons
		Timer::after_millis(200).await;
	}
}
