pub mod note;
pub mod partition;

use embassy_nrf::{
	gpio::AnyPin,
	peripherals::PWM0,
	pwm::{Prescaler, SimplePwm},
};
use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, signal::Signal};
use embassy_time::{Duration, Timer};
use note::{Duration::Eighth, Volume, NOTES_PER_SEC};
use partition::Partition;

pub static SND_SIG: Signal<CriticalSectionRawMutex, Partition> = Signal::new();
const PWM_CHAN: usize = 0;

#[embassy_executor::task]
pub async fn task(spkr_pin: AnyPin, pwm: PWM0) {
	let mut spkr = SimplePwm::new_1ch(pwm, spkr_pin);
	let volume = Volume::Mid;

	spkr.set_prescaler(Prescaler::Div1);
	spkr.set_period(1);

	loop {
		let notes = SND_SIG.wait().await;

		spkr.enable();

		for (height, octave, duration) in notes {
			// Note height
			spkr.set_period(*height as u32 * *octave as u32);

			// Note volume
			spkr.set_duty(
				PWM_CHAN,
				match height {
					note::Height::P => 0,
					_ => spkr.max_duty() / 100 * volume as u16,
				},
			);
			Timer::after(Duration::from_hz(NOTES_PER_SEC * *duration as u64)).await;

			// Separate notes
			spkr.set_duty(PWM_CHAN, 0);
			Timer::after(Duration::from_hz(NOTES_PER_SEC * Eighth as u64)).await;
		}

		spkr.disable();
	}
}
