#[derive(Default)]
pub enum State {
	#[default]
	Waiting,
	Ongoing,
	Lost,
	Won,
}
