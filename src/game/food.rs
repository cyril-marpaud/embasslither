use core::ops::{Deref, DerefMut};

use defmt::unwrap;
use embassy_nrf::rng::Rng;
use rand::Rng as _;

use super::{snake::Snake, Coords};
use crate::{
	display::image::{COLS, ROWS},
	RNG,
};

pub struct Food(pub Coords, Rng<'static, RNG>);

impl Food {
	pub fn new(rng: Rng<'static, RNG>) -> Self {
		Self(Coords::CENTER, rng)
	}

	pub fn regenerate(&mut self, snake: &Snake) {
		let size = ROWS * COLS;
		let mut available = (0..size)
			.map(|i| Coords {
				x: i / ROWS,
				y: i % ROWS,
			})
			.filter(|c| !snake.contains(c));

		let rand = self.1.gen_range(0..size - snake.len());
		self.0 = unwrap!(available.nth(rand));
	}

	pub fn reset(&mut self) {
		self.0 = Coords::CENTER;
	}
}

impl Deref for Food {
	type Target = Coords;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for Food {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}
