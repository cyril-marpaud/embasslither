use core::ops::{Deref, DerefMut};

use defmt::unwrap;
use enum_iterator::{next_cycle, previous_cycle, Sequence};
use heapless::Vec;

use super::{food::Food, state::State, Coords};
use crate::{
	display::image::{COLS, ROWS},
	sound::{
		partition::{BEEP, BOOP},
		SND_SIG,
	},
};

#[derive(Default, Sequence)]
pub(crate) enum Direction {
	Up,
	#[default]
	Right,
	Down,
	Left,
}

pub struct Snake(Vec<Coords, { ROWS * COLS }>, Direction);

impl Snake {
	pub fn slither(&mut self, food: &mut Food) -> Option<State> {
		let head = unwrap!(self.last());

		let new_head = match self.1 {
			Direction::Up => Coords {
				x: head.x.wrapping_sub(1).min(ROWS - 1),
				y: head.y,
			},
			Direction::Right => Coords {
				x: head.x,
				y: head.y.wrapping_add(1) % COLS,
			},
			Direction::Down => Coords {
				x: head.x.wrapping_add(1) % ROWS,
				y: head.y,
			},
			Direction::Left => Coords {
				x: head.x,
				y: head.y.wrapping_sub(1).min(COLS - 1),
			},
		};

		let mut ate_food = false;

		// Did not eat food -> remove the tail
		if new_head != food.0 {
			self.remove(0);
		} else {
			ate_food = true;
		}

		// Gonna eat tail -> loose
		if self.contains(&new_head) {
			Some(State::Lost)
		// Not gonna eat tail & length = MAX-1 -> win
		} else if self.len() == COLS * ROWS - 1 {
			Some(State::Won)
		} else {
			unwrap!(self.push(new_head));
			if ate_food {
				food.regenerate(self);
				SND_SIG.signal(BEEP);
			} else {
				SND_SIG.signal(BOOP)
			}
			None
		}
	}

	pub fn turn_left(&mut self) {
		self.1 = previous_cycle(&self.1);
	}

	pub fn turn_right(&mut self) {
		self.1 = next_cycle(&self.1);
	}
}

impl Default for Snake {
	fn default() -> Self {
		let mut snake = Vec::default();
		unwrap!(snake.push(Coords::default()));
		Self(snake, Direction::default())
	}
}

impl Deref for Snake {
	type Target = Vec<Coords, { ROWS * COLS }>;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for Snake {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}
